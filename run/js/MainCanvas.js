function MainCanvas () {

    function explodeAsteroid (asteroid) {

        var speedX = asteroid.speedX(),
            speedY = asteroid.speedY()

        var asteroidX = asteroid.x,
            asteroidY = asteroid.y,
            size = asteroid.size

        var multiplier = size * 0.008,
            particleSize = Math.pow(size * 14, 0.22)
        for (var k = 0; k < size * 4; k++) {
            var angle = Math.random() * Math.PI * 2,
                x = Math.cos(angle) * multiplier,
                y = Math.sin(angle) * multiplier
            particles.push(Particle(asteroidX + x, asteroidY + y, speedX, speedY, particleSize))
        }

        asteroidExplosions.push(AsteroidExplosion(asteroidX, asteroidY, size))

    }

    function explodeRepairKit (repairKit) {
        repairKitExplosions.push(RepairKitExplosion(repairKit.x, repairKit.y))
    }

    function explodeWeaponUpgrade (weaponUpgrade) {
        weaponUpgradeExplosions.push(WeaponUpgradeExplosion(weaponUpgrade.x, weaponUpgrade.y))
    }

    function hitAsteroid (asteroid, object) {

        var speedX = asteroid.speedX(),
            speedY = asteroid.speedY()

        if (asteroid.hit()) {

            var objectX = object.x,
                objectY = object.y

            for (var k = 0; k < 4; k++) {
                particles.push(Particle(objectX, objectY, speedX, speedY, 1.3))
            }

            return false

        }

        explodeAsteroid(asteroid)

        var size = asteroid.size
        if (size > 1) {

            var asteroidX = asteroid.x,
                asteroidY = asteroid.y

            var multiplier = size * 0.006
            var halfSize = size / 2
            for (var k = 0; k < 3; k++) {
                var angle = Math.random() * Math.PI * 2,
                    x = Math.cos(angle) * multiplier,
                    y = Math.sin(angle) * multiplier
                asteroids.push(Asteroid(asteroidX + x, asteroidY + y, halfSize, scale, canvasWidth, canvasHeight, speedX, speedY))
            }

            if (level > 1 && Math.random() < 0.2) {
                repairKits.push(RepairKit(asteroidX, asteroidY, speedX, speedY, scale))
            }

            if (level > 2 && Math.random() < 0.2) {
                weaponUpgrades.push(WeaponUpgrade(asteroidX, asteroidY, speedX, speedY, scale))
            }

        } else {
            if (asteroids.length == 1) levelClear.show()
        }

        return true

    }

    function nextLevel () {
        levelClear.hide()
        level++
        plane.upgradeWeapon()
        for (var k = 0; k < level; k++) {
            pushBiggestAsteroid()
        }
    }

    function pushBiggestAsteroid () {

        var size = 8
        var radius = size * 0.01

        var x, y
        if (Math.random() < 0.5) {
            x = (Math.random() * 2 - 1) * halfCanvasWidth / scale
            y = halfCanvasHeight / scale + radius
            if (Math.random() < 0.5) y = -y
        } else {
            y = (Math.random() * 2 - 1) * -halfCanvasHeight / scale
            x = halfCanvasWidth / scale + radius
            if (Math.random() < 0.5) x = -x
        }

        x += plane.x
        y += plane.y

        asteroids.push(Asteroid(x, y, size, scale, canvasWidth, canvasHeight, plane.speedX(), plane.speedY()))

    }

    function resize () {

        devicePixelRatio = window.devicePixelRatio

        canvasWidth = innerWidth * devicePixelRatio
        canvasHeight = innerHeight * devicePixelRatio

        halfCanvasWidth = canvasWidth / 2
        halfCanvasHeight = canvasHeight / 2

        scale = (canvasWidth + canvasHeight) / 2

        buttonSmallSpacing = scale * 0.05
        buttonBigSpacing = scale * 0.14
        buttonRadius = scale * 0.09
        plane.resize(scale)
        healthBar.resize(scale, canvasWidth)
        cloudBackground.resize(scale, canvasWidth, canvasHeight)
        farStarBackground.resize(scale, canvasWidth, canvasHeight)
        nearStarBackground.resize(scale, canvasWidth, canvasHeight)
        scoreLabel.resize(scale)
        levelLabel.resize(scale)
        start.resize(scale, canvasWidth, canvasHeight)
        gameOver.resize(scale, canvasWidth, canvasHeight)
        levelClear.resize(scale, canvasWidth, canvasHeight)

        directionControl.resize(scale, devicePixelRatio, canvasWidth, canvasHeight)
        shootButton.resize(buttonRadius, canvasWidth - buttonSmallSpacing - buttonRadius, canvasHeight - buttonBigSpacing - buttonRadius)
        gasButton.resize(buttonRadius, canvasWidth - buttonRadius - buttonBigSpacing, canvasHeight - buttonRadius - buttonSmallSpacing)

        canvas.style.transform = 'scale(' + (1 / devicePixelRatio) + ')'
        canvas.width = canvasWidth
        canvas.height = canvasHeight
        canvas.style.left = -halfCanvasWidth + 'px'
        canvas.style.top = -halfCanvasHeight + 'px'
        repaint()

        for (var i in asteroids) {
            asteroids[i].resize(scale, canvasWidth, canvasHeight)
        }

        for (var i in repairKits) {
            repairKits[i].resize(scale)
        }

        for (var i in weaponUpgrades) {
            weaponUpgrades[i].resize(scale)
        }

        for (var i in bullets) bullets[i].resize(scale)

    }

    function repaint () {
        if (animationFrame !== null) return
        animationFrame = requestAnimationFrame(function () {

            animationFrame = null

            c.save()

            c.fillStyle = backgroundColor
            c.fillRect(0, 0, canvasWidth, canvasHeight)

            c.save()
            c.translate(halfCanvasWidth, halfCanvasHeight)

            var planeX = plane.x,
                planeY = plane.y

            cloudBackground.paint(c, planeX, planeY)
            farStarBackground.paint(c, planeX, planeY)
            nearStarBackground.paint(c, planeX, planeY)
            plane.paint(c)

            c.translate(-planeX * scale, -planeY * scale)

            for (var i in asteroidExplosions) asteroidExplosions[i].paint(c, scale)
            for (var i in repairKitExplosions) repairKitExplosions[i].paint(c, scale)
            for (var i in weaponUpgradeExplosions) weaponUpgradeExplosions[i].paint(c, scale)
            for (var i in repairKits) repairKits[i].paint(c)
            for (var i in weaponUpgrades) weaponUpgrades[i].paint(c)

            c.beginPath()
            for (var i in bullets) bullets[i].paint(c)
            for (var i in asteroids) asteroids[i].paint(c)
            c.fillStyle = '#fff'
            c.fill()

            for (var i in particles) particles[i].paint(c, scale)
            c.restore()

            healthBar.paint(c, plane.lifeRatio())
            start.paint(c)
            c.save()
            c.globalCompositeOperation = 'lighter'
            directionControl.paint(c)
            shootButton.paint(c, buttonRadius)
            gasButton.paint(c, buttonRadius)
            c.restore()
            scoreLabel.paint(c, score, scoreMultiplier)
            levelLabel.paint(c, level)
            gameOver.paint(c)
            levelClear.paint(c)

            c.restore()

        })
    }

    function restart () {
        plane.reset()
        while (asteroids.length) explodeAsteroid(asteroids.shift())
        while (repairKits.length) explodeRepairKit(repairKits.shift())
        while (weaponUpgrades.length) explodeWeaponUpgrade(weaponUpgrades.shift())
        level = 1
        score = 0
        pushBiggestAsteroid()
    }

    function startTick () {
        tickInterval = setInterval(tick, 30)
    }

    function tick () {

        shootButton.release()
        gasButton.release()

        for (var i = 0; i < touches.length; i++) {
            var touch = touches[i],
                touchX = touch.clientX * devicePixelRatio,
                touchY = touch.clientY * devicePixelRatio
            shootButton.press(touchX, touchY)
            gasButton.press(touchX, touchY)
        }

        var rotateCcwPressed = keys[37],
            rotateCwPressed = keys[39],
            shootPressed = keys[32] || shootButton.pressed(),
            gasPressed = keys[38] || gasButton.pressed()

        for (var k = 0; k < 2; k++) {

            if (scoreMultiplierLife) {
                scoreMultiplierLife--
                if (!scoreMultiplierLife) {
                    scoreMultiplier = 1
                }
            }

            if (rotateCcwPressed) {
                if (!rotateCwPressed) plane.rotateCw()
            } else if (rotateCwPressed) {
                plane.rotateCcw()
            }

            var angle = directionControl.angle()
            if (angle !== null) plane.setAngle(angle)

            var planeX = plane.x,
                planeY = plane.y

            var planeHit = false,
                planeDead = plane.dead(),
                asteroidDead = false

            if (!planeDead) {

                for (var i = 0; i < repairKits.length; i++) {
                    var repairKit = repairKits[i]
                    if (Collide(plane, repairKit)) {
                        plane.repair()
                        repairKits.splice(i, 1)
                        i--
                        explodeRepairKit(repairKit)
                    }
                }

                for (var i = 0; i < weaponUpgrades.length; i++) {
                    var weaponUpgrade = weaponUpgrades[i]
                    if (Collide(plane, weaponUpgrade)) {
                        plane.upgradeWeapon()
                        weaponUpgrades.splice(i, 1)
                        i--
                        explodeWeaponUpgrade(weaponUpgrade)
                    }
                }

            }

            for (var i = 0; i < asteroids.length; i++) {

                var asteroid = asteroids[i]

                var speedX = asteroid.speedX(),
                    speedY = asteroid.speedY()

                if (!planeHit && !planeDead && Collide(plane, asteroid)) {
                    planeHit = true
                    if (plane.hit(particles, speedX, speedY)) {
                        if (hitAsteroid(asteroid, plane)) {
                            asteroidDead = true
                            asteroids.splice(i, 1)
                            i--
                        }
                        if (plane.dead()) {
                            planeDead = true
                            for (var j = 0; j < 50; j++) {
                                particles.push(Particle(planeX, planeY, speedX, speedY, 4))
                            }
                        }
                    }
                }

                if (!asteroidDead) {
                    for (var j = 0; j < bullets.length; j++) {
                        var bullet = bullets[j]
                        if (Collide(bullet, asteroid)) {

                            score += scoreMultiplier
                            scoreMultiplier++
                            scoreMultiplierLife = 30

                            bullets.splice(j, 1)
                            j--

                            if (hitAsteroid(asteroid, bullet)) {
                                asteroids.splice(i, 1)
                                i--
                                break
                            }

                        }
                    }
                }

            }

            plane.gas(gasPressed)

            for (var i = 0; i < asteroidExplosions.length; i++) {
                if (!asteroidExplosions[i].tick()) {
                    asteroidExplosions.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < repairKitExplosions.length; i++) {
                if (!repairKitExplosions[i].tick()) {
                    repairKitExplosions.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < weaponUpgradeExplosions.length; i++) {
                if (!weaponUpgradeExplosions[i].tick()) {
                    weaponUpgradeExplosions.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < asteroids.length; i++) {
                asteroids[i].tick(planeX, planeY)
            }

            for (var i = 0; i < repairKits.length; i++) {
                if (!repairKits[i].tick(planeX, planeY)) {
                    repairKits.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < weaponUpgrades.length; i++) {
                if (!weaponUpgrades[i].tick(planeX, planeY)) {
                    weaponUpgrades.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < bullets.length; i++) {
                if (!bullets[i].tick()) {
                    bullets.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < particles.length; i++) {
                if (!particles[i].tick()) {
                    particles.splice(i, 1)
                    i--
                }
            }

            if (!plane.dead()) plane.tick()

            if (shootPressed) plane.shoot(bullets)

            gameOver.tick(plane.dead(), score)

        }

        levelClear.tick()
        start.tick()
        repaint()

    }

    var start = Start()

    var gameOver = GameOver()

    var levelClear = LevelClear()

    var healthBar = HealthBar()

    var scoreMultiplier = 1,
        scoreMultiplierLife = 0

    var scoreLabel = ScoreLabel(),
        levelLabel = LevelLabel()

    var backgroundHue = Math.random() * 360,
        backgroundSaturation = 10 + Math.random() * 40,
        backgroundLuminance = 10 +  Math.random() * 20,
        backgroundColor = 'hsla(' + backgroundHue + ', ' + backgroundSaturation + '%, ' + backgroundLuminance + '%, 0.95)'

    var requestAnimationFrame = window.requestAnimationFrame
    if (!requestAnimationFrame) {
        requestAnimationFrame = window.mozRequestAnimationFrame
    }
    if (!requestAnimationFrame) {
        requestAnimationFrame = function (callback) {
            setTimeout(callback, 0)
        }
    }

    var score = 0,
        level = 1

    var particles = []

    var repairKitExplosions = []

    var weaponUpgradeExplosions = []

    var repairKits = []

    var weaponUpgrades = []

    var asteroidExplosions = []

    var asteroids = []

    var bullets = []

    var buttonSmallSpacing, buttonBigSpacing, buttonRadius

    var canvasWidth, canvasHeight
    var halfCanvasWidth, halfCanvasHeight
    var devicePixelRatio, scale

    var animationFrame = null

    var plane = Plane()

    var shootButton = Button(0),
        gasButton = Button(200)

    var canvas = document.createElement('canvas')
    canvas.className = 'MainCanvas-canvas'

    var c = canvas.getContext('2d')

    var cloudBackground = CloudBackground(backgroundHue,
        backgroundSaturation, backgroundLuminance, 10)

    var farStarBackground = StarBackground(9)

    var nearStarBackground = StarBackground(8)

    var directionControl = DirectionControl(canvas)

    var element = document.createElement('div')
    element.className = 'MainCanvas'
    element.appendChild(canvas)

    var touches = []
    addEventListener('touchstart', function (e) {
        e.preventDefault()
        var changedTouches = e.changedTouches
        for (var i = 0; i < changedTouches.length; i++) {
            touches.push(changedTouches[i])
        }
        if (gameOver.canHide) restart()
        if (levelClear.canHide) nextLevel()
        start.hide()
    })
    addEventListener('touchmove', function (e) {
        e.preventDefault()
        var changedTouches = e.changedTouches
        for (var i = 0; i < changedTouches.length; i++) {
            var changedTouch = changedTouches[i]
            for (var j = 0; j < touches.length; j++) {
                if (changedTouch.identifier === touches[j].identifier) {
                    touches[j] = changedTouch
                    break
                }
            }
        }
    })
    addEventListener('touchend', function (e) {
        e.preventDefault()
        var changedTouches = e.changedTouches
        for (var i = 0; i < changedTouches.length; i++) {
            for (var j = 0; j < touches.length; j++) {
                if (changedTouches[i].identifier === touches[j].identifier) {
                    touches.splice(j, 1)
                    j--
                    break
                }
            }
        }
    })

    var keys = {}
    addEventListener('keydown', function (e) {
        keys[e.keyCode] = true
        if (gameOver.canHide) restart()
        if (levelClear.canHide) nextLevel()
        start.hide()
    })
    addEventListener('keyup', function (e) {
        keys[e.keyCode] = false
    })

    addEventListener('resize', resize)
    resize()
    plane.gas(false)

    pushBiggestAsteroid()

    var tickInterval
    startTick()
    tick()

    setTimeout(start.hide, 3000)

    return {
        element: element,
        resume: startTick,
        pause: function () {
            clearInterval(tickInterval)
        },
    }

}
