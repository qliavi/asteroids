function Bullet (x, y, angle, speedX, speedY, scale) {

    function updateRect () {
        rectX = -halfWidth * scale
        rectY = -halfHeight * scale
        rectWidth = width * scale
        rectHeight = height * scale
    }

    var speedMultiplier = 0.0065
    speedX += Math.cos(angle) * speedMultiplier
    speedY += Math.sin(angle) * speedMultiplier

    var life = 80,
        fadeLife = 25

    var width = 0.012,
        height = 0.002

    var halfWidth = width / 2,
        halfHeight = height / 2

    var rectX, rectY, rectWidth, rectHeight

    updateRect()

    var that = {
        radius: Math.min(width + height) / 2,
        x: x,
        y: y,
        paint: function (c) {
            c.save()
            c.translate(x * scale, y * scale)
            c.rotate(angle)
            if (life < fadeLife) c.globalAlpha = life / fadeLife
            c.rect(rectX, rectY, rectWidth, rectHeight)
            c.restore()
        },
        resize: function (_scale) {
            scale = _scale
            updateRect()
        },
        tick: function () {

            x += speedX
            y += speedY

            that.x = x
            that.y = y

            life--
            return life

        },
    }

    return that

}
