function Flame (width, height) {

    var canvas = document.createElement('canvas')
    canvas.width = Math.ceil(width)
    canvas.height = Math.ceil(height)

    var halfHeight = height / 2

    var c = canvas.getContext('2d')
    c.translate(width, halfHeight)

    var gradient = c.createRadialGradient(0, 0, 0, 0, 0, Math.max(width, halfHeight))
    gradient.addColorStop(0.1, '#fff')
    gradient.addColorStop(0.2, 'rgba(255, 255, 255, 0.9)')
    gradient.addColorStop(0.5, 'rgba(0, 255, 255, 0.8)')
    gradient.addColorStop(1, 'rgba(0, 0, 255, 0)')

    c.rect(-width, -halfHeight, width, height)
    if (width > halfHeight) c.scale(1, halfHeight / width)
    else c.scale(width / halfHeight, 1)
    c.fillStyle = gradient
    c.fill()

    return {
        canvas: canvas,
        paint: function (c) {
            c.drawImage(canvas, -width, -halfHeight)
        },
    }

}
