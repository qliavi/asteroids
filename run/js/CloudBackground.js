function CloudBackground (backgroundHue, backgroundSaturation, backgroundLuminance, distance) {

    var clouds = []
    for (var i = 0; i < 50 * distance; i++) {
        clouds.push({
            x: Math.random() - 0.5,
            y: Math.random() - 0.5,
            radius: (0.05 + Math.random() * 0.95) / (distance - 6),
        })
    }

    var scale
    var canvasWidth, canvasHeight
    var halfCanvasWidth, halfCanvasHeight

    var fillStyle

    return {
        paint: function (c, x, y) {

            x /= distance
            y /= distance

            c.save()
            c.translate(-x * scale, -y * scale)
            c.fillStyle = fillStyle
            c.fillRect(x * scale - halfCanvasWidth, y * scale - halfCanvasHeight, canvasWidth, canvasHeight)
            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {

            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            halfCanvasHeight = canvasHeight / 2

            var size = Math.ceil(scale)

            var canvas = document.createElement('canvas')
            canvas.width = canvas.height = size

            var c = canvas.getContext('2d')

            var time = Date.now()

            for (var i = 0; i < clouds.length; i++) {

                var cloud = clouds[i],
                    visualRadius = cloud.radius * scale * 0.3

                var hue = backgroundHue + (Math.random() - 0.5) * 100,
                    saturation = backgroundSaturation + (Math.random() - 0.5) * 10,
                    luminance = backgroundLuminance + (Math.random() - 0.5) * 30

                c.fillStyle = 'hsla(' + hue + ', ' + saturation + '%, ' + luminance + '%, 0.02)'

                for (var j = -1; j <= 1; j++) {
                    for (var k = -1; k <= 1; k++) {
                        var x = cloud.x * size + j * size,
                            y = cloud.y * size + k * size
                        c.beginPath()
                        c.arc(x, y, visualRadius, 0, Math.PI * 2)
                        c.fill()
                    }
                }

                if (Date.now() - time > 300) break

            }

            fillStyle = c.createPattern(canvas, 'repeat')

        },
    }

}
