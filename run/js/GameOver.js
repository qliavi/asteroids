function GameOver () {

    var life = 0
    var maxLife = 30

    var scale,
        fontSize

    var smallFont,
        mediumFont,
        largeFont

    var canvasWidth,
        canvasHeight

    var halfCanvasWidth,
        halfCanvasHeight

    var animationWidth

    var score = 0

    var highScore = parseInt(localStorage.highScore, 10)
    if (!isFinite(highScore)) highScore = 0

    var isHighScore = false

    var that = {
        canHide: false,
        paint: function (c) {

            if (!life) return

            var lifeRatio = life / maxLife,
                invertedLifeRatio = 1 - lifeRatio

            c.save()
            c.globalAlpha = lifeRatio

            c.fillStyle = 'rgba(0, 0, 0, 0.8)'
            c.fillRect(0, 0, canvasWidth, canvasHeight)

            c.translate(halfCanvasWidth, halfCanvasHeight)

            c.fillStyle = '#fff'
            c.textAlign = 'center'
            c.textBaseline = 'middle'

            c.translate(0, fontSize * -1.5)
            c.font = smallFont
            c.fillText('YOUR SCORE', -invertedLifeRatio * animationWidth, 0)
            c.translate(0, fontSize)

            c.font = largeFont
            c.fillText(score, invertedLifeRatio * animationWidth, 0)
            c.translate(0, fontSize)

            if (isHighScore) {
                c.translate(0, fontSize / 2)
                c.font = mediumFont
                c.fillStyle = '#fc0'
                c.fillText('NEW RECORD!', -invertedLifeRatio * animationWidth, 0)
            } else {

                c.font = smallFont
                c.fillText('HIGH SCORE', -invertedLifeRatio * animationWidth, 0)
                c.translate(0, fontSize)

                c.font = largeFont
                c.fillText(highScore, invertedLifeRatio * animationWidth, 0)

            }

            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {
            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            halfCanvasHeight = canvasHeight / 2
            fontSize = scale * 0.1
            smallFont = (fontSize * 0.5) + 'px sans-serif'
            mediumFont = 'bold ' + (fontSize * 0.7) + 'px sans-serif'
            largeFont = 'bold ' + fontSize + 'px sans-serif'
            animationWidth = scale * 0.2
        },
        tick: function (planeDead, _score) {
            that.canHide = false
            if (planeDead) {

                if (!life) {
                    score = _score
                    if (score > highScore) {
                        isHighScore = true
                        localStorage.highScore = highScore = score
                    }
                }

                if (life < maxLife) life++
                else that.canHide = true

            } else {
                if (life) life--
                else isHighScore = false
            }
        },
    }

    return that

}
