function WeaponUpgradeExplosion (x, y) {

    var radius = 0.2

    var life = 0,
        maxLife = 40

    return {
        paint: function (c, scale) {
            c.save()
            c.translate(x * scale, y * scale)
            c.beginPath()
            c.globalAlpha = Math.pow(1 - life / maxLife, 3) * 0.3
            c.arc(0, 0, scale * radius * life / maxLife, 0, Math.PI * 2)
            c.fillStyle = '#f50'
            c.fill()
            c.restore()
        },
        tick: function () {
            life++
            return life < maxLife
        },
    }

}
