function Collide (object1, object2) {

    var minDistance = object1.radius + object2.radius

    var dx = object1.x - object2.x,
        dy = object1.y - object2.y

    var distance = Math.sqrt(dx * dx + dy * dy)
    return distance < minDistance

}
