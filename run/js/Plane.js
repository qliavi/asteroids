function Plane () {

    function reset () {
        var randomAngle = Math.random() * Math.PI * 2,
            multiplier = 0.0008
        speedX = Math.cos(randomAngle) * multiplier,
        speedY = Math.sin(randomAngle) * multiplier
        life = maxLife
        weaponIndex = 0
        weapon = weapons[0]
    }

    var width, top, bottom
    var flameX, flameY

    var flames, normalFlames, gasFlames

    var angle = -Math.PI / 2,
        angleIncrement = 0,
        hitAngleIncrement = 0,
        angleStep = 0.003,
        maxAngleIncrement = 0.05,
        gasMultiplier = 0.000065

    var x = 0, y = 0

    var speedX, speedY

    var gas = false

    var life, maxLife = 20

    var maxHitWait = 5,
        hitWait = 0

    var destAngle = null

    var maxShakeMultiplier = 40,
        shakeMultiplier = 0

    var shakeX = 0,
        shakeY = 0

    var scale

    var weapons = [SimpleWeapon(), AlternatingWeapon(), DoubleWeapon()]

    var weapon, weaponIndex

    reset()

    var that = {
        radius: 0.04,
        reset: reset,
        x: x,
        y: y,
        angle: function () {
            return angle
        },
        dead: function () {
            return life == 0
        },
        lifeRatio: function () {
            return life / maxLife
        },
        hit: function (particles, objectSpeedX, objectSpeedY) {

            if (hitWait) return false

            if (weaponIndex > 0) {
                weaponIndex--
                weapon = weapons[weaponIndex]
            }

            hitWait = maxHitWait
            hitAngleIncrement = (Math.random() > 0.5 ? 1 : -1) * (1 + Math.random()) * 0.1
            life--
            shakeMultiplier = maxShakeMultiplier

            var particleSpeedX = (objectSpeedX + speedX) / 2,
                particleSpeedY = (objectSpeedY + speedY) / 2

            if (life > 0) {
                for (var j = 0; j < 4; j++) {
                    particles.push(Particle(x, y, particleSpeedX, particleSpeedY, 3))
                }
            }

            return true

        },
        gas: function (_gas) {
            gas = _gas
            flames = gas ? gasFlames : normalFlames
        },
        paint: function (c) {

            if (!life) return

            c.save()
            c.translate(-shakeX * scale, -shakeY * scale)
            c.rotate(angle)

            var flame = flames[Math.floor(Math.random() * flames.length)]
        
            c.save()
            c.translate(flameX, -flameY)
            flame.paint(c)
            c.restore()

            c.save()
            c.translate(flameX, flameY)
            flame.paint(c)
            c.restore()

            c.beginPath()
            c.fillStyle = '#fff'
            c.moveTo(top * 2 / 3, 0)
            c.lineTo(bottom, width)
            c.lineTo(bottom, -width)
            c.closePath()
            c.fill()

            c.restore()

        },
        repair: function () {
            life = Math.min(maxLife, life + 5)
        },
        resize: function (_scale) {
            scale = _scale
            width = scale * 0.026
            top = scale * 0.08
            bottom = -scale * 0.022
            flameX = -scale * 0.019
            flameY = scale * 0.012
            normalFlames = [
                Flame(scale * 0.009, scale * 0.018),
                Flame(scale * 0.011, scale * 0.018),
                Flame(scale * 0.013, scale * 0.018),
            ]
            gasFlames = [
                Flame(scale * 0.035, scale * 0.024),
                Flame(scale * 0.041, scale * 0.0245),
                Flame(scale * 0.047, scale * 0.025),
            ]
        },
        rotateCcw: function () {
            angleIncrement = Math.min(angleIncrement + angleStep, maxAngleIncrement)
            destAngle = null
        },
        rotateCw: function () {
            angleIncrement = Math.max(angleIncrement - angleStep, -maxAngleIncrement)
            destAngle = null
        },
        setAngle: function (_angle) {
            destAngle = _angle
            if (destAngle - angle > Math.PI) {
                destAngle -= Math.PI * 2
            } else if (destAngle - angle < -Math.PI) {
                destAngle += Math.PI * 2
            }
        },
        shoot: function (bullets) {
            weapon.shoot(bullets, x, y, angle, speedX, speedY, scale)
        },
        speedX: function () {
            return speedX
        },
        speedY: function () {
            return speedY
        },
        tick: function () {

            if (hitWait) hitWait--

            if (shakeMultiplier > 0) {

                var shakeLevel = 0.01 * shakeMultiplier / maxShakeMultiplier,
                    shakeAngle = Math.random() * Math.PI * 2

                x -= shakeX
                y -= shakeY
                shakeX = shakeLevel * Math.cos(shakeAngle)
                shakeY = shakeLevel * Math.sin(shakeAngle)
                x += shakeX
                y += shakeY

                shakeMultiplier--

            } else {
                shakeX = 0
                shakeY = 0
            }

            weapon.tick()

            if (destAngle === null) angle += angleIncrement
            else angle = (destAngle * 0.08 + angle * 0.92)
            angleIncrement *= 0.95

            angle += hitAngleIncrement
            hitAngleIncrement *= 0.95

            if (gas) {
                speedX += Math.cos(angle) * gasMultiplier
                speedY += Math.sin(angle) * gasMultiplier
            }

            x += speedX
            y += speedY

            that.x = x
            that.y = y

        },
        upgradeWeapon: function () {
            if (weaponIndex < weapons.length - 1) {
                weaponIndex++
                weapon = weapons[weaponIndex]
            }
        },
    }

    return that

}
