function Start () {

    var maxLife = 15,
        life = maxLife

    var scale,
        canvasWidth,
        canvasHeight,
        halfCanvasWidth,
        font

    var hiding = false

    return {
        hide: function () {
            hiding = true
        },
        paint: function (c) {

            if (!life) return

            c.save()
            c.globalAlpha = life / maxLife

            c.fillStyle = 'rgba(0, 0, 0, 0.8)'
            c.fillRect(0, 0, canvasWidth, canvasHeight)

            c.translate(halfCanvasWidth, 0)

            c.fillStyle = '#fff'
            c.textAlign = 'center'
            c.textBaseline = 'middle'
            c.font = font
            c.fillText('DIRECTION', -halfCanvasWidth + scale * 0.18, canvasHeight - scale * 0.175)
            c.fillText('FIRE', halfCanvasWidth - scale * 0.14, canvasHeight - scale * 0.225)
            c.fillText('GAS', halfCanvasWidth - scale * 0.23, canvasHeight - scale * 0.135)

            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {
            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            font = 'bold ' + (scale * 0.034) + 'px sans-serif'
        },
        tick: function () {
            if (hiding && life) life--
        },
    }

}
