function Button (hue) {

    var x, y, radius

    var pressed = false

    var pressedFillStyle = 'hsla(' + hue + ', 60%, 50%, 0.4)',
        pressedStrokeStyle = 'hsla(' + hue + ', 100%, 50%, 0.6)'

    var fillStyle = releasedFillStyle

    var releasedFillStyle = 'hsla(' + hue + ', 60%, 50%, 0.1)',
        releasedStrokeStyle = 'hsla(' + hue + ', 100%, 50%, 0.4)'

    var strokeStyle = releasedStrokeStyle

    return {
        paint: function (c) {
            c.save()
            c.translate(x, y)
            c.beginPath()
            c.arc(0, 0, radius, 0, 2 * Math.PI)
            c.fillStyle = fillStyle
            c.fill()
            c.strokeStyle = strokeStyle
            c.lineWidth = radius * 0.05
            c.stroke()
            c.restore()
        },
        press: function (clientX, clientY) {

            if (pressed) return

            var dx = clientX - x,
                dy = clientY - y

            pressed = Math.sqrt(dx * dx + dy * dy) < radius

            if (pressed) {
                fillStyle = pressedFillStyle
                strokeStyle = pressedStrokeStyle
            }

        },
        pressed: function () {
            return pressed
        },
        release: function () {
            pressed = false
            fillStyle = releasedFillStyle
            strokeStyle = releasedStrokeStyle
        },
        resize: function (_radius, _x, _y) {
            x = _x
            y = _y
            radius = _radius
        },
    }

}
