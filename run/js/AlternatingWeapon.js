function AlternatingWeapon () {

    var wait = 0
    var maxWait = 8

    var distance = 0.014

    var side = 1

    return {
        shoot: function (bullets, x, y, angle, speedX, speedY, scale) {

            if (wait) return
            wait = maxWait

            var sideAngle = angle + side * Math.PI / 2
            x += Math.cos(sideAngle) * distance
            y += Math.sin(sideAngle) * distance

            bullets.push(Bullet(x, y, angle, speedX, speedY, scale))

            var multiplier = 0.00006
            speedX -= Math.cos(angle) * multiplier
            speedY -= Math.sin(angle) * multiplier

            side = -side

        },
        tick: function () {
            if (wait) wait--
        },
    }

}
