function Particle (x, y, speedX, speedY, radius) {

    var randomAngle = Math.random() * Math.PI * 2,
        multiplier = 0.008 * radius
    x += Math.cos(randomAngle) * multiplier
    y += Math.sin(randomAngle) * multiplier

    var randomAngle = Math.random() * Math.PI * 2,
        multiplier = 0.0017 * Math.pow(radius, 0.2)
    speedX += Math.cos(randomAngle) * multiplier,
    speedY += Math.sin(randomAngle) * multiplier

    var maxLife = Math.ceil(radius * 20),
        life = maxLife,
        fadeLife = Math.ceil(life / 3)

    radius *= 0.0015

    return {
        paint: function (c, scale) {

            c.save()
            c.translate(x * scale, y * scale)

            if (life < fadeLife) c.globalAlpha = life / fadeLife

            c.beginPath()
            c.arc(0, 0, radius * scale * life / maxLife, 0, Math.PI * 2)
            c.fillStyle = '#fff'
            c.fill()

            c.restore()

        },
        tick: function () {

            x += speedX
            y += speedY

            life--
            return life

        },
    }

}
