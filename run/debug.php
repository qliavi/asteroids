<?php

include_once 'fns/echo_html.php';
echo_html(
    '<html>',
    '<link rel="stylesheet" type="text/css" href="css/Main.css" />'
    .'<link rel="stylesheet" type="text/css" href="css/MainCanvas.css" />',
    '<script type="text/javascript" src="js/AlternatingWeapon.js"></script>'
    .'<script type="text/javascript" src="js/Asteroid.js"></script>'
    .'<script type="text/javascript" src="js/AsteroidExplosion.js"></script>'
    .'<script type="text/javascript" src="js/Bullet.js"></script>'
    .'<script type="text/javascript" src="js/Button.js"></script>'
    .'<script type="text/javascript" src="js/CloudBackground.js"></script>'
    .'<script type="text/javascript" src="js/Collide.js"></script>'
    .'<script type="text/javascript" src="js/DirectionControl.js"></script>'
    .'<script type="text/javascript" src="js/DoubleWeapon.js"></script>'
    .'<script type="text/javascript" src="js/Flame.js"></script>'
    .'<script type="text/javascript" src="js/GameOver.js"></script>'
    .'<script type="text/javascript" src="js/HealthBar.js"></script>'
    .'<script type="text/javascript" src="js/LevelClear.js"></script>'
    .'<script type="text/javascript" src="js/LevelLabel.js"></script>'
    .'<script type="text/javascript" src="js/MainCanvas.js"></script>'
    .'<script type="text/javascript" src="js/Particle.js"></script>'
    .'<script type="text/javascript" src="js/Plane.js"></script>'
    .'<script type="text/javascript" src="js/RepairKit.js"></script>'
    .'<script type="text/javascript" src="js/RepairKitExplosion.js"></script>'
    .'<script type="text/javascript" src="js/ScoreLabel.js"></script>'
    .'<script type="text/javascript" src="js/SimpleWeapon.js"></script>'
    .'<script type="text/javascript" src="js/StarBackground.js"></script>'
    .'<script type="text/javascript" src="js/Start.js"></script>'
    .'<script type="text/javascript" src="js/WeaponUpgrade.js"></script>'
    .'<script type="text/javascript" src="js/WeaponUpgradeExplosion.js"></script>'
    .'<script type="text/javascript" src="js/Main.js"></script>'
);
