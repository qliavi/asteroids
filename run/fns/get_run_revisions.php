<?php

function get_run_revisions () {
    return [
        'compressed.css' => 4,
        'compressed.js' => 10,
    ];
}
