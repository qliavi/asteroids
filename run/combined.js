(function () {
function AlternatingWeapon () {

    var wait = 0
    var maxWait = 8

    var distance = 0.014

    var side = 1

    return {
        shoot: function (bullets, x, y, angle, speedX, speedY, scale) {

            if (wait) return
            wait = maxWait

            var sideAngle = angle + side * Math.PI / 2
            x += Math.cos(sideAngle) * distance
            y += Math.sin(sideAngle) * distance

            bullets.push(Bullet(x, y, angle, speedX, speedY, scale))

            var multiplier = 0.00006
            speedX -= Math.cos(angle) * multiplier
            speedY -= Math.sin(angle) * multiplier

            side = -side

        },
        tick: function () {
            if (wait) wait--
        },
    }

}
;
function Asteroid (x, y, size, scale, canvasWidth, canvasHeight, speedX, speedY) {

    var randomAngle = Math.random() * Math.PI * 2,
        speedMultiplier = 0.003 / size
    speedX += Math.cos(randomAngle) * speedMultiplier,
    speedY += Math.sin(randomAngle) * speedMultiplier

    var maxLife = size,
        life = maxLife

    var radius = size * 0.01,
        scaledRadius = radius * scale

    var maxShakeMultiplier = 40,
        shakeMultiplier = 0

    var visualX, visualY

    var that = {
        radius: radius,
        size: size,
        x: x,
        y: y,
        hit: function () {
            shakeMultiplier = maxShakeMultiplier
            life--
            return life > 0
        },
        paint: function (c) {
            c.save()
            c.translate(visualX * scale, visualY * scale)
            c.moveTo(scaledRadius, 0)
            c.arc(0, 0, scaledRadius, 0, Math.PI * 2)
            c.restore()
        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {
            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            scaledRadius = radius * scale
        },
        speedX: function () {
            return speedX
        },
        speedY: function () {
            return speedY
        },
        tick: function (planeX, planeY) {

            if (shakeMultiplier > 0) {

                var shakeLevel = Math.pow(1 - life / maxLife, 2) * 0.01 * shakeMultiplier / maxShakeMultiplier,
                    shakeAngle = Math.random() * Math.PI * 2

                visualX = x + shakeLevel * Math.cos(shakeAngle)
                visualY = y + shakeLevel * Math.sin(shakeAngle)

                shakeMultiplier--

            } else {
                visualX = x
                visualY = y
            }

            x += speedX
            y += speedY

            var sceneWidth = canvasWidth / scale,
                edgeX = sceneWidth / 2 + radius

            if (x - planeX > edgeX) {
                x -= sceneWidth + radius * 2
            }

            if (x - planeX < -edgeX) {
                x += sceneWidth + radius * 2
            }

            var sceneHeight = canvasHeight / scale,
                edgeY = sceneHeight / 2 + radius

            if (y - planeY > edgeY) {
                y -= sceneHeight + radius * 2
            }

            if (y - planeY < -edgeY) {
                y += sceneHeight + radius * 2
            }

            that.x = x
            that.y = y

        },
    }

    return that

}
;
function AsteroidExplosion (x, y, size) {

    var radius = size * 0.2

    var life = 0,
        maxLife = (size + 6) * 5

    return {
        paint: function (c, scale) {
            c.save()
            c.translate(x * scale, y * scale)
            c.beginPath()
            c.globalAlpha = Math.pow(1 - life / maxLife, 3) * 0.3
            c.arc(0, 0, scale * radius * life / maxLife, 0, Math.PI * 2)
            c.fillStyle = '#fff'
            c.fill()
            c.restore()
        },
        tick: function () {
            life++
            return life < maxLife
        },
    }

}
;
function Bullet (x, y, angle, speedX, speedY, scale) {

    function updateRect () {
        rectX = -halfWidth * scale
        rectY = -halfHeight * scale
        rectWidth = width * scale
        rectHeight = height * scale
    }

    var speedMultiplier = 0.0065
    speedX += Math.cos(angle) * speedMultiplier
    speedY += Math.sin(angle) * speedMultiplier

    var life = 80,
        fadeLife = 25

    var width = 0.012,
        height = 0.002

    var halfWidth = width / 2,
        halfHeight = height / 2

    var rectX, rectY, rectWidth, rectHeight

    updateRect()

    var that = {
        radius: Math.min(width + height) / 2,
        x: x,
        y: y,
        paint: function (c) {
            c.save()
            c.translate(x * scale, y * scale)
            c.rotate(angle)
            if (life < fadeLife) c.globalAlpha = life / fadeLife
            c.rect(rectX, rectY, rectWidth, rectHeight)
            c.restore()
        },
        resize: function (_scale) {
            scale = _scale
            updateRect()
        },
        tick: function () {

            x += speedX
            y += speedY

            that.x = x
            that.y = y

            life--
            return life

        },
    }

    return that

}
;
function Button (hue) {

    var x, y, radius

    var pressed = false

    var pressedFillStyle = 'hsla(' + hue + ', 60%, 50%, 0.4)',
        pressedStrokeStyle = 'hsla(' + hue + ', 100%, 50%, 0.6)'

    var fillStyle = releasedFillStyle

    var releasedFillStyle = 'hsla(' + hue + ', 60%, 50%, 0.1)',
        releasedStrokeStyle = 'hsla(' + hue + ', 100%, 50%, 0.4)'

    var strokeStyle = releasedStrokeStyle

    return {
        paint: function (c) {
            c.save()
            c.translate(x, y)
            c.beginPath()
            c.arc(0, 0, radius, 0, 2 * Math.PI)
            c.fillStyle = fillStyle
            c.fill()
            c.strokeStyle = strokeStyle
            c.lineWidth = radius * 0.05
            c.stroke()
            c.restore()
        },
        press: function (clientX, clientY) {

            if (pressed) return

            var dx = clientX - x,
                dy = clientY - y

            pressed = Math.sqrt(dx * dx + dy * dy) < radius

            if (pressed) {
                fillStyle = pressedFillStyle
                strokeStyle = pressedStrokeStyle
            }

        },
        pressed: function () {
            return pressed
        },
        release: function () {
            pressed = false
            fillStyle = releasedFillStyle
            strokeStyle = releasedStrokeStyle
        },
        resize: function (_radius, _x, _y) {
            x = _x
            y = _y
            radius = _radius
        },
    }

}
;
function CloudBackground (backgroundHue, backgroundSaturation, backgroundLuminance, distance) {

    var clouds = []
    for (var i = 0; i < 50 * distance; i++) {
        clouds.push({
            x: Math.random() - 0.5,
            y: Math.random() - 0.5,
            radius: (0.05 + Math.random() * 0.95) / (distance - 6),
        })
    }

    var scale
    var canvasWidth, canvasHeight
    var halfCanvasWidth, halfCanvasHeight

    var fillStyle

    return {
        paint: function (c, x, y) {

            x /= distance
            y /= distance

            c.save()
            c.translate(-x * scale, -y * scale)
            c.fillStyle = fillStyle
            c.fillRect(x * scale - halfCanvasWidth, y * scale - halfCanvasHeight, canvasWidth, canvasHeight)
            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {

            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            halfCanvasHeight = canvasHeight / 2

            var size = Math.ceil(scale)

            var canvas = document.createElement('canvas')
            canvas.width = canvas.height = size

            var c = canvas.getContext('2d')

            var time = Date.now()

            for (var i = 0; i < clouds.length; i++) {

                var cloud = clouds[i],
                    visualRadius = cloud.radius * scale * 0.3

                var hue = backgroundHue + (Math.random() - 0.5) * 100,
                    saturation = backgroundSaturation + (Math.random() - 0.5) * 10,
                    luminance = backgroundLuminance + (Math.random() - 0.5) * 30

                c.fillStyle = 'hsla(' + hue + ', ' + saturation + '%, ' + luminance + '%, 0.02)'

                for (var j = -1; j <= 1; j++) {
                    for (var k = -1; k <= 1; k++) {
                        var x = cloud.x * size + j * size,
                            y = cloud.y * size + k * size
                        c.beginPath()
                        c.arc(x, y, visualRadius, 0, Math.PI * 2)
                        c.fill()
                    }
                }

                if (Date.now() - time > 300) break

            }

            fillStyle = c.createPattern(canvas, 'repeat')

        },
    }

}
;
function Collide (object1, object2) {

    var minDistance = object1.radius + object2.radius

    var dx = object1.x - object2.x,
        dy = object1.y - object2.y

    var distance = Math.sqrt(dx * dx + dy * dy)
    return distance < minDistance

}
;
function DirectionControl (canvas) {

    function updateAngle () {

        controlX = touchX - x,
        controlY = touchY - y

        angle = Math.atan(controlY / controlX)
        if (controlX < 0) angle += Math.PI

        var distance = Math.sqrt(controlX * controlX + controlY * controlY)
        if (distance > radius) distance = radius

        controlX = Math.cos(angle) * distance
        controlY = Math.sin(angle) * distance

    }

    var scale, devicePixelRatio

    var angle = null
    var x, y, radius

    var identifier = null
    var touchX, touchY

    var controlX = 0,
        controlY = 0

    canvas.addEventListener('touchend', function (e) {
        e.preventDefault()
        if (identifier === null) return
        var touches = e.changedTouches
        for (var i = 0; i < touches.length; i++) {
            if (touches[i].identifier === identifier) {
                identifier = null
                angle = null
                controlX = controlY = 0
                break
            }
        }
    })
    canvas.addEventListener('touchmove', function (e) {
        e.preventDefault()
        if (identifier === null) return
        var touches = e.changedTouches
        for (var i = 0; i < touches.length; i++) {
            var touch = touches[i]
            if (touch.identifier === identifier) {
                touchX = touch.clientX * devicePixelRatio
                touchY = touch.clientY * devicePixelRatio
                updateAngle()
                break
            }
        }
    })
    canvas.addEventListener('touchstart', function (e) {
        e.preventDefault()
        if (identifier !== null) return
        var touches = e.changedTouches
        for (var i = 0; i < touches.length; i++) {

            var touch = touches[i],
                clientX = touch.clientX * devicePixelRatio,
                clientY = touch.clientY * devicePixelRatio

            var dx = clientX - x,
                dy = clientY - y

            if (Math.sqrt(dx * dx + dy * dy) < radius) {
                identifier = touch.identifier
                touchX = clientX
                touchY = clientY
                updateAngle()
                break
            }

        }
    })

    return {
        angle: function () {
            return angle
        },
        paint: function (c) {

            c.save()
            c.translate(x, y)

            c.lineWidth = scale * 0.004
            if (identifier === null) {
                c.fillStyle = 'hsla(60, 60%, 50%, 0.1)'
                c.strokeStyle = 'hsla(60, 100%, 50%, 0.4)'
            } else {
                c.fillStyle = 'hsla(60, 60%, 50%, 0.4)'
                c.strokeStyle = 'hsla(60, 100%, 50%, 0.6)'
            }

            c.beginPath()
            c.arc(0, 0, radius, 0, Math.PI * 2)
            c.fill()
            c.stroke()

            c.beginPath()
            c.arc(controlX, controlY, radius / 3, 0, Math.PI * 2)
            c.fill()
            c.stroke()

            c.restore()

        },
        resize: function (_scale, _devicePixelRatio, canvasWidth, canvasHeight) {
            scale = _scale
            devicePixelRatio = _devicePixelRatio
            radius = scale * 0.13
            x = scale * 0.05 + radius
            y = canvasHeight - (scale * 0.05 + radius)
            if (identifier !== null) updateAngle()
        },
    }

}
;
function DoubleWeapon () {

    var wait = 0
    var maxWait = 9

    var distance = 0.014

    return {
        shoot: function (bullets, x, y, angle, speedX, speedY, scale) {

            if (wait) return
            wait = maxWait

            var sideAngle = angle + Math.PI / 2,
                bulletX = x + Math.cos(sideAngle) * distance
                bulletY = y + Math.sin(sideAngle) * distance
            bullets.push(Bullet(bulletX, bulletY, angle, speedX, speedY, scale))

            var sideAngle = angle - Math.PI / 2,
                bulletX = x + Math.cos(sideAngle) * distance,
                bulletY = y + Math.sin(sideAngle) * distance
            bullets.push(Bullet(bulletX, bulletY, angle, speedX, speedY, scale))

            var multiplier = 0.00006
            speedX -= Math.cos(angle) * multiplier
            speedY -= Math.sin(angle) * multiplier

        },
        tick: function () {
            if (wait) wait--
        },
    }

}
;
function Flame (width, height) {

    var canvas = document.createElement('canvas')
    canvas.width = Math.ceil(width)
    canvas.height = Math.ceil(height)

    var halfHeight = height / 2

    var c = canvas.getContext('2d')
    c.translate(width, halfHeight)

    var gradient = c.createRadialGradient(0, 0, 0, 0, 0, Math.max(width, halfHeight))
    gradient.addColorStop(0.1, '#fff')
    gradient.addColorStop(0.2, 'rgba(255, 255, 255, 0.9)')
    gradient.addColorStop(0.5, 'rgba(0, 255, 255, 0.8)')
    gradient.addColorStop(1, 'rgba(0, 0, 255, 0)')

    c.rect(-width, -halfHeight, width, height)
    if (width > halfHeight) c.scale(1, halfHeight / width)
    else c.scale(width / halfHeight, 1)
    c.fillStyle = gradient
    c.fill()

    return {
        canvas: canvas,
        paint: function (c) {
            c.drawImage(canvas, -width, -halfHeight)
        },
    }

}
;
function GameOver () {

    var life = 0
    var maxLife = 30

    var scale,
        fontSize

    var smallFont,
        mediumFont,
        largeFont

    var canvasWidth,
        canvasHeight

    var halfCanvasWidth,
        halfCanvasHeight

    var animationWidth

    var score = 0

    var highScore = parseInt(localStorage.highScore, 10)
    if (!isFinite(highScore)) highScore = 0

    var isHighScore = false

    var that = {
        canHide: false,
        paint: function (c) {

            if (!life) return

            var lifeRatio = life / maxLife,
                invertedLifeRatio = 1 - lifeRatio

            c.save()
            c.globalAlpha = lifeRatio

            c.fillStyle = 'rgba(0, 0, 0, 0.8)'
            c.fillRect(0, 0, canvasWidth, canvasHeight)

            c.translate(halfCanvasWidth, halfCanvasHeight)

            c.fillStyle = '#fff'
            c.textAlign = 'center'
            c.textBaseline = 'middle'

            c.translate(0, fontSize * -1.5)
            c.font = smallFont
            c.fillText('YOUR SCORE', -invertedLifeRatio * animationWidth, 0)
            c.translate(0, fontSize)

            c.font = largeFont
            c.fillText(score, invertedLifeRatio * animationWidth, 0)
            c.translate(0, fontSize)

            if (isHighScore) {
                c.translate(0, fontSize / 2)
                c.font = mediumFont
                c.fillStyle = '#fc0'
                c.fillText('NEW RECORD!', -invertedLifeRatio * animationWidth, 0)
            } else {

                c.font = smallFont
                c.fillText('HIGH SCORE', -invertedLifeRatio * animationWidth, 0)
                c.translate(0, fontSize)

                c.font = largeFont
                c.fillText(highScore, invertedLifeRatio * animationWidth, 0)

            }

            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {
            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            halfCanvasHeight = canvasHeight / 2
            fontSize = scale * 0.1
            smallFont = (fontSize * 0.5) + 'px sans-serif'
            mediumFont = 'bold ' + (fontSize * 0.7) + 'px sans-serif'
            largeFont = 'bold ' + fontSize + 'px sans-serif'
            animationWidth = scale * 0.2
        },
        tick: function (planeDead, _score) {
            that.canHide = false
            if (planeDead) {

                if (!life) {
                    score = _score
                    if (score > highScore) {
                        isHighScore = true
                        localStorage.highScore = highScore = score
                    }
                }

                if (life < maxLife) life++
                else that.canHide = true

            } else {
                if (life) life--
                else isHighScore = false
            }
        },
    }

    return that

}
;
function HealthBar () {

    var x, y, width, height

    return {
        paint: function (c, lifeRatio) {

            c.fillStyle = 'rgba(255, 255, 255, 0.1)'
            c.fillRect(x, y, width, height)

            c.fillStyle = 'hsla(' + Math.floor(lifeRatio * 120) + ', 100%, 50%, 0.8)'
            c.fillRect(x, y, width * lifeRatio, height)

        },
        resize: function (scale, canvasWidth) {
            x = scale * 0.05
            y = scale * 0.05
            width = canvasWidth - 0.10 * scale
            height = scale * 0.01
        },
    }

}
;
function LevelClear () {

    var life = 0
    var lifeDirection = 0
    var maxLife = 15

    var scale,
        fontSize

    var font

    var canvasWidth,
        canvasHeight

    var halfCanvasWidth,
        halfCanvasHeight

    var animationWidth

    var that = {
        canHide: false,
        hide: function () {
            lifeDirection = -1
        },
        paint: function (c) {

            if (!life) return

            var lifeRatio = life / maxLife,
                invertedLifeRatio = 1 - lifeRatio

            c.save()
            c.globalAlpha = lifeRatio

            c.fillStyle = 'rgba(0, 0, 0, 0.8)'
            c.fillRect(0, 0, canvasWidth, canvasHeight)

            c.translate(halfCanvasWidth, halfCanvasHeight)

            c.fillStyle = '#fff'
            c.textAlign = 'center'
            c.textBaseline = 'middle'

            c.font = font
            c.fillText('LEVEL CLEAR', -invertedLifeRatio * animationWidth, 0)

            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {
            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            halfCanvasHeight = canvasHeight / 2
            fontSize = scale * 0.05
            font = 'bold ' + fontSize + 'px sans-serif'
            animationWidth = scale * 0.2
        },
        show: function () {
            lifeDirection = 1
        },
        tick: function () {

            life += lifeDirection

            if (life == maxLife) {
                that.canHide = true
                lifeDirection = 0
            } else {
                that.canHide = false
                if (!life) lifeDirection = 0
            }

        },
    }

    return that

}
;
function LevelLabel () {

    var x, y, font, labelCanvas

    var textX, textY
    var verticalAlign = 0.55

    var color = 'rgba(153, 153, 153, 0.6)'

    return {
        paint: function (c, level) {

            var text = level

            c.drawImage(labelCanvas, x, y)
            c.fillStyle = color
            c.font = font
            c.textBaseline = 'middle'
            c.fillText(text, textX, textY)

        },
        resize: function (scale) {

            x = scale * 0.05
            y = scale * 0.16

            var fontSize = scale * 0.04
            font = 'bold ' + fontSize + 'px monospace'

            labelCanvas = (function () {

                var padding = 1,
                    text = 'LEVEL'

                var canvas = document.createElement('canvas')
                canvas.height = Math.ceil(fontSize + padding * 2)

                var font = fontSize + 'px monospace'

                var c = canvas.getContext('2d')
                c.font = font
                canvas.width = c.measureText(text).width + padding * 2
                c.fillStyle = 'rgba(128, 128, 128, 0.6)'
                c.font = font
                c.textBaseline = 'middle'
                c.fillText(text, padding, canvas.height * verticalAlign)

                return canvas

            })()

            textX = x + labelCanvas.width + scale * 0.02
            textY = y + labelCanvas.height * verticalAlign

        },
    }

}
;
function MainCanvas () {

    function explodeAsteroid (asteroid) {

        var speedX = asteroid.speedX(),
            speedY = asteroid.speedY()

        var asteroidX = asteroid.x,
            asteroidY = asteroid.y,
            size = asteroid.size

        var multiplier = size * 0.008,
            particleSize = Math.pow(size * 14, 0.22)
        for (var k = 0; k < size * 4; k++) {
            var angle = Math.random() * Math.PI * 2,
                x = Math.cos(angle) * multiplier,
                y = Math.sin(angle) * multiplier
            particles.push(Particle(asteroidX + x, asteroidY + y, speedX, speedY, particleSize))
        }

        asteroidExplosions.push(AsteroidExplosion(asteroidX, asteroidY, size))

    }

    function explodeRepairKit (repairKit) {
        repairKitExplosions.push(RepairKitExplosion(repairKit.x, repairKit.y))
    }

    function explodeWeaponUpgrade (weaponUpgrade) {
        weaponUpgradeExplosions.push(WeaponUpgradeExplosion(weaponUpgrade.x, weaponUpgrade.y))
    }

    function hitAsteroid (asteroid, object) {

        var speedX = asteroid.speedX(),
            speedY = asteroid.speedY()

        if (asteroid.hit()) {

            var objectX = object.x,
                objectY = object.y

            for (var k = 0; k < 4; k++) {
                particles.push(Particle(objectX, objectY, speedX, speedY, 1.3))
            }

            return false

        }

        explodeAsteroid(asteroid)

        var size = asteroid.size
        if (size > 1) {

            var asteroidX = asteroid.x,
                asteroidY = asteroid.y

            var multiplier = size * 0.006
            var halfSize = size / 2
            for (var k = 0; k < 3; k++) {
                var angle = Math.random() * Math.PI * 2,
                    x = Math.cos(angle) * multiplier,
                    y = Math.sin(angle) * multiplier
                asteroids.push(Asteroid(asteroidX + x, asteroidY + y, halfSize, scale, canvasWidth, canvasHeight, speedX, speedY))
            }

            if (level > 1 && Math.random() < 0.2) {
                repairKits.push(RepairKit(asteroidX, asteroidY, speedX, speedY, scale))
            }

            if (level > 2 && Math.random() < 0.2) {
                weaponUpgrades.push(WeaponUpgrade(asteroidX, asteroidY, speedX, speedY, scale))
            }

        } else {
            if (asteroids.length == 1) levelClear.show()
        }

        return true

    }

    function nextLevel () {
        levelClear.hide()
        level++
        plane.upgradeWeapon()
        for (var k = 0; k < level; k++) {
            pushBiggestAsteroid()
        }
    }

    function pushBiggestAsteroid () {

        var size = 8
        var radius = size * 0.01

        var x, y
        if (Math.random() < 0.5) {
            x = (Math.random() * 2 - 1) * halfCanvasWidth / scale
            y = halfCanvasHeight / scale + radius
            if (Math.random() < 0.5) y = -y
        } else {
            y = (Math.random() * 2 - 1) * -halfCanvasHeight / scale
            x = halfCanvasWidth / scale + radius
            if (Math.random() < 0.5) x = -x
        }

        x += plane.x
        y += plane.y

        asteroids.push(Asteroid(x, y, size, scale, canvasWidth, canvasHeight, plane.speedX(), plane.speedY()))

    }

    function resize () {

        devicePixelRatio = window.devicePixelRatio

        canvasWidth = innerWidth * devicePixelRatio
        canvasHeight = innerHeight * devicePixelRatio

        halfCanvasWidth = canvasWidth / 2
        halfCanvasHeight = canvasHeight / 2

        scale = (canvasWidth + canvasHeight) / 2

        buttonSmallSpacing = scale * 0.05
        buttonBigSpacing = scale * 0.14
        buttonRadius = scale * 0.09
        plane.resize(scale)
        healthBar.resize(scale, canvasWidth)
        cloudBackground.resize(scale, canvasWidth, canvasHeight)
        farStarBackground.resize(scale, canvasWidth, canvasHeight)
        nearStarBackground.resize(scale, canvasWidth, canvasHeight)
        scoreLabel.resize(scale)
        levelLabel.resize(scale)
        start.resize(scale, canvasWidth, canvasHeight)
        gameOver.resize(scale, canvasWidth, canvasHeight)
        levelClear.resize(scale, canvasWidth, canvasHeight)

        directionControl.resize(scale, devicePixelRatio, canvasWidth, canvasHeight)
        shootButton.resize(buttonRadius, canvasWidth - buttonSmallSpacing - buttonRadius, canvasHeight - buttonBigSpacing - buttonRadius)
        gasButton.resize(buttonRadius, canvasWidth - buttonRadius - buttonBigSpacing, canvasHeight - buttonRadius - buttonSmallSpacing)

        canvas.style.transform = 'scale(' + (1 / devicePixelRatio) + ')'
        canvas.width = canvasWidth
        canvas.height = canvasHeight
        canvas.style.left = -halfCanvasWidth + 'px'
        canvas.style.top = -halfCanvasHeight + 'px'
        repaint()

        for (var i in asteroids) {
            asteroids[i].resize(scale, canvasWidth, canvasHeight)
        }

        for (var i in repairKits) {
            repairKits[i].resize(scale)
        }

        for (var i in weaponUpgrades) {
            weaponUpgrades[i].resize(scale)
        }

        for (var i in bullets) bullets[i].resize(scale)

    }

    function repaint () {
        if (animationFrame !== null) return
        animationFrame = requestAnimationFrame(function () {

            animationFrame = null

            c.save()

            c.fillStyle = backgroundColor
            c.fillRect(0, 0, canvasWidth, canvasHeight)

            c.save()
            c.translate(halfCanvasWidth, halfCanvasHeight)

            var planeX = plane.x,
                planeY = plane.y

            cloudBackground.paint(c, planeX, planeY)
            farStarBackground.paint(c, planeX, planeY)
            nearStarBackground.paint(c, planeX, planeY)
            plane.paint(c)

            c.translate(-planeX * scale, -planeY * scale)

            for (var i in asteroidExplosions) asteroidExplosions[i].paint(c, scale)
            for (var i in repairKitExplosions) repairKitExplosions[i].paint(c, scale)
            for (var i in weaponUpgradeExplosions) weaponUpgradeExplosions[i].paint(c, scale)
            for (var i in repairKits) repairKits[i].paint(c)
            for (var i in weaponUpgrades) weaponUpgrades[i].paint(c)

            c.beginPath()
            for (var i in bullets) bullets[i].paint(c)
            for (var i in asteroids) asteroids[i].paint(c)
            c.fillStyle = '#fff'
            c.fill()

            for (var i in particles) particles[i].paint(c, scale)
            c.restore()

            healthBar.paint(c, plane.lifeRatio())
            start.paint(c)
            c.save()
            c.globalCompositeOperation = 'lighter'
            directionControl.paint(c)
            shootButton.paint(c, buttonRadius)
            gasButton.paint(c, buttonRadius)
            c.restore()
            scoreLabel.paint(c, score, scoreMultiplier)
            levelLabel.paint(c, level)
            gameOver.paint(c)
            levelClear.paint(c)

            c.restore()

        })
    }

    function restart () {
        plane.reset()
        while (asteroids.length) explodeAsteroid(asteroids.shift())
        while (repairKits.length) explodeRepairKit(repairKits.shift())
        while (weaponUpgrades.length) explodeWeaponUpgrade(weaponUpgrades.shift())
        level = 1
        score = 0
        pushBiggestAsteroid()
    }

    function startTick () {
        tickInterval = setInterval(tick, 30)
    }

    function tick () {

        shootButton.release()
        gasButton.release()

        for (var i = 0; i < touches.length; i++) {
            var touch = touches[i],
                touchX = touch.clientX * devicePixelRatio,
                touchY = touch.clientY * devicePixelRatio
            shootButton.press(touchX, touchY)
            gasButton.press(touchX, touchY)
        }

        var rotateCcwPressed = keys[37],
            rotateCwPressed = keys[39],
            shootPressed = keys[32] || shootButton.pressed(),
            gasPressed = keys[38] || gasButton.pressed()

        for (var k = 0; k < 2; k++) {

            if (scoreMultiplierLife) {
                scoreMultiplierLife--
                if (!scoreMultiplierLife) {
                    scoreMultiplier = 1
                }
            }

            if (rotateCcwPressed) {
                if (!rotateCwPressed) plane.rotateCw()
            } else if (rotateCwPressed) {
                plane.rotateCcw()
            }

            var angle = directionControl.angle()
            if (angle !== null) plane.setAngle(angle)

            var planeX = plane.x,
                planeY = plane.y

            var planeHit = false,
                planeDead = plane.dead(),
                asteroidDead = false

            if (!planeDead) {

                for (var i = 0; i < repairKits.length; i++) {
                    var repairKit = repairKits[i]
                    if (Collide(plane, repairKit)) {
                        plane.repair()
                        repairKits.splice(i, 1)
                        i--
                        explodeRepairKit(repairKit)
                    }
                }

                for (var i = 0; i < weaponUpgrades.length; i++) {
                    var weaponUpgrade = weaponUpgrades[i]
                    if (Collide(plane, weaponUpgrade)) {
                        plane.upgradeWeapon()
                        weaponUpgrades.splice(i, 1)
                        i--
                        explodeWeaponUpgrade(weaponUpgrade)
                    }
                }

            }

            for (var i = 0; i < asteroids.length; i++) {

                var asteroid = asteroids[i]

                var speedX = asteroid.speedX(),
                    speedY = asteroid.speedY()

                if (!planeHit && !planeDead && Collide(plane, asteroid)) {
                    planeHit = true
                    if (plane.hit(particles, speedX, speedY)) {
                        if (hitAsteroid(asteroid, plane)) {
                            asteroidDead = true
                            asteroids.splice(i, 1)
                            i--
                        }
                        if (plane.dead()) {
                            planeDead = true
                            for (var j = 0; j < 50; j++) {
                                particles.push(Particle(planeX, planeY, speedX, speedY, 4))
                            }
                        }
                    }
                }

                if (!asteroidDead) {
                    for (var j = 0; j < bullets.length; j++) {
                        var bullet = bullets[j]
                        if (Collide(bullet, asteroid)) {

                            score += scoreMultiplier
                            scoreMultiplier++
                            scoreMultiplierLife = 30

                            bullets.splice(j, 1)
                            j--

                            if (hitAsteroid(asteroid, bullet)) {
                                asteroids.splice(i, 1)
                                i--
                                break
                            }

                        }
                    }
                }

            }

            plane.gas(gasPressed)

            for (var i = 0; i < asteroidExplosions.length; i++) {
                if (!asteroidExplosions[i].tick()) {
                    asteroidExplosions.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < repairKitExplosions.length; i++) {
                if (!repairKitExplosions[i].tick()) {
                    repairKitExplosions.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < weaponUpgradeExplosions.length; i++) {
                if (!weaponUpgradeExplosions[i].tick()) {
                    weaponUpgradeExplosions.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < asteroids.length; i++) {
                asteroids[i].tick(planeX, planeY)
            }

            for (var i = 0; i < repairKits.length; i++) {
                if (!repairKits[i].tick(planeX, planeY)) {
                    repairKits.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < weaponUpgrades.length; i++) {
                if (!weaponUpgrades[i].tick(planeX, planeY)) {
                    weaponUpgrades.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < bullets.length; i++) {
                if (!bullets[i].tick()) {
                    bullets.splice(i, 1)
                    i--
                }
            }

            for (var i = 0; i < particles.length; i++) {
                if (!particles[i].tick()) {
                    particles.splice(i, 1)
                    i--
                }
            }

            if (!plane.dead()) plane.tick()

            if (shootPressed) plane.shoot(bullets)

            gameOver.tick(plane.dead(), score)

        }

        levelClear.tick()
        start.tick()
        repaint()

    }

    var start = Start()

    var gameOver = GameOver()

    var levelClear = LevelClear()

    var healthBar = HealthBar()

    var scoreMultiplier = 1,
        scoreMultiplierLife = 0

    var scoreLabel = ScoreLabel(),
        levelLabel = LevelLabel()

    var backgroundHue = Math.random() * 360,
        backgroundSaturation = 10 + Math.random() * 40,
        backgroundLuminance = 10 +  Math.random() * 20,
        backgroundColor = 'hsla(' + backgroundHue + ', ' + backgroundSaturation + '%, ' + backgroundLuminance + '%, 0.95)'

    var requestAnimationFrame = window.requestAnimationFrame
    if (!requestAnimationFrame) {
        requestAnimationFrame = window.mozRequestAnimationFrame
    }
    if (!requestAnimationFrame) {
        requestAnimationFrame = function (callback) {
            setTimeout(callback, 0)
        }
    }

    var score = 0,
        level = 1

    var particles = []

    var repairKitExplosions = []

    var weaponUpgradeExplosions = []

    var repairKits = []

    var weaponUpgrades = []

    var asteroidExplosions = []

    var asteroids = []

    var bullets = []

    var buttonSmallSpacing, buttonBigSpacing, buttonRadius

    var canvasWidth, canvasHeight
    var halfCanvasWidth, halfCanvasHeight
    var devicePixelRatio, scale

    var animationFrame = null

    var plane = Plane()

    var shootButton = Button(0),
        gasButton = Button(200)

    var canvas = document.createElement('canvas')
    canvas.className = 'MainCanvas-canvas'

    var c = canvas.getContext('2d')

    var cloudBackground = CloudBackground(backgroundHue,
        backgroundSaturation, backgroundLuminance, 10)

    var farStarBackground = StarBackground(9)

    var nearStarBackground = StarBackground(8)

    var directionControl = DirectionControl(canvas)

    var element = document.createElement('div')
    element.className = 'MainCanvas'
    element.appendChild(canvas)

    var touches = []
    addEventListener('touchstart', function (e) {
        e.preventDefault()
        var changedTouches = e.changedTouches
        for (var i = 0; i < changedTouches.length; i++) {
            touches.push(changedTouches[i])
        }
        if (gameOver.canHide) restart()
        if (levelClear.canHide) nextLevel()
        start.hide()
    })
    addEventListener('touchmove', function (e) {
        e.preventDefault()
        var changedTouches = e.changedTouches
        for (var i = 0; i < changedTouches.length; i++) {
            var changedTouch = changedTouches[i]
            for (var j = 0; j < touches.length; j++) {
                if (changedTouch.identifier === touches[j].identifier) {
                    touches[j] = changedTouch
                    break
                }
            }
        }
    })
    addEventListener('touchend', function (e) {
        e.preventDefault()
        var changedTouches = e.changedTouches
        for (var i = 0; i < changedTouches.length; i++) {
            for (var j = 0; j < touches.length; j++) {
                if (changedTouches[i].identifier === touches[j].identifier) {
                    touches.splice(j, 1)
                    j--
                    break
                }
            }
        }
    })

    var keys = {}
    addEventListener('keydown', function (e) {
        keys[e.keyCode] = true
        if (gameOver.canHide) restart()
        if (levelClear.canHide) nextLevel()
        start.hide()
    })
    addEventListener('keyup', function (e) {
        keys[e.keyCode] = false
    })

    addEventListener('resize', resize)
    resize()
    plane.gas(false)

    pushBiggestAsteroid()

    var tickInterval
    startTick()
    tick()

    setTimeout(start.hide, 3000)

    return {
        element: element,
        resume: startTick,
        pause: function () {
            clearInterval(tickInterval)
        },
    }

}
;
function Particle (x, y, speedX, speedY, radius) {

    var randomAngle = Math.random() * Math.PI * 2,
        multiplier = 0.008 * radius
    x += Math.cos(randomAngle) * multiplier
    y += Math.sin(randomAngle) * multiplier

    var randomAngle = Math.random() * Math.PI * 2,
        multiplier = 0.0017 * Math.pow(radius, 0.2)
    speedX += Math.cos(randomAngle) * multiplier,
    speedY += Math.sin(randomAngle) * multiplier

    var maxLife = Math.ceil(radius * 20),
        life = maxLife,
        fadeLife = Math.ceil(life / 3)

    radius *= 0.0015

    return {
        paint: function (c, scale) {

            c.save()
            c.translate(x * scale, y * scale)

            if (life < fadeLife) c.globalAlpha = life / fadeLife

            c.beginPath()
            c.arc(0, 0, radius * scale * life / maxLife, 0, Math.PI * 2)
            c.fillStyle = '#fff'
            c.fill()

            c.restore()

        },
        tick: function () {

            x += speedX
            y += speedY

            life--
            return life

        },
    }

}
;
function Plane () {

    function reset () {
        var randomAngle = Math.random() * Math.PI * 2,
            multiplier = 0.0008
        speedX = Math.cos(randomAngle) * multiplier,
        speedY = Math.sin(randomAngle) * multiplier
        life = maxLife
        weaponIndex = 0
        weapon = weapons[0]
    }

    var width, top, bottom
    var flameX, flameY

    var flames, normalFlames, gasFlames

    var angle = -Math.PI / 2,
        angleIncrement = 0,
        hitAngleIncrement = 0,
        angleStep = 0.003,
        maxAngleIncrement = 0.05,
        gasMultiplier = 0.000065

    var x = 0, y = 0

    var speedX, speedY

    var gas = false

    var life, maxLife = 20

    var maxHitWait = 5,
        hitWait = 0

    var destAngle = null

    var maxShakeMultiplier = 40,
        shakeMultiplier = 0

    var shakeX = 0,
        shakeY = 0

    var scale

    var weapons = [SimpleWeapon(), AlternatingWeapon(), DoubleWeapon()]

    var weapon, weaponIndex

    reset()

    var that = {
        radius: 0.04,
        reset: reset,
        x: x,
        y: y,
        angle: function () {
            return angle
        },
        dead: function () {
            return life == 0
        },
        lifeRatio: function () {
            return life / maxLife
        },
        hit: function (particles, objectSpeedX, objectSpeedY) {

            if (hitWait) return false

            if (weaponIndex > 0) {
                weaponIndex--
                weapon = weapons[weaponIndex]
            }

            hitWait = maxHitWait
            hitAngleIncrement = (Math.random() > 0.5 ? 1 : -1) * (1 + Math.random()) * 0.1
            life--
            shakeMultiplier = maxShakeMultiplier

            var particleSpeedX = (objectSpeedX + speedX) / 2,
                particleSpeedY = (objectSpeedY + speedY) / 2

            if (life > 0) {
                for (var j = 0; j < 4; j++) {
                    particles.push(Particle(x, y, particleSpeedX, particleSpeedY, 3))
                }
            }

            return true

        },
        gas: function (_gas) {
            gas = _gas
            flames = gas ? gasFlames : normalFlames
        },
        paint: function (c) {

            if (!life) return

            c.save()
            c.translate(-shakeX * scale, -shakeY * scale)
            c.rotate(angle)

            var flame = flames[Math.floor(Math.random() * flames.length)]
        
            c.save()
            c.translate(flameX, -flameY)
            flame.paint(c)
            c.restore()

            c.save()
            c.translate(flameX, flameY)
            flame.paint(c)
            c.restore()

            c.beginPath()
            c.fillStyle = '#fff'
            c.moveTo(top * 2 / 3, 0)
            c.lineTo(bottom, width)
            c.lineTo(bottom, -width)
            c.closePath()
            c.fill()

            c.restore()

        },
        repair: function () {
            life = Math.min(maxLife, life + 5)
        },
        resize: function (_scale) {
            scale = _scale
            width = scale * 0.026
            top = scale * 0.08
            bottom = -scale * 0.022
            flameX = -scale * 0.019
            flameY = scale * 0.012
            normalFlames = [
                Flame(scale * 0.009, scale * 0.018),
                Flame(scale * 0.011, scale * 0.018),
                Flame(scale * 0.013, scale * 0.018),
            ]
            gasFlames = [
                Flame(scale * 0.035, scale * 0.024),
                Flame(scale * 0.041, scale * 0.0245),
                Flame(scale * 0.047, scale * 0.025),
            ]
        },
        rotateCcw: function () {
            angleIncrement = Math.min(angleIncrement + angleStep, maxAngleIncrement)
            destAngle = null
        },
        rotateCw: function () {
            angleIncrement = Math.max(angleIncrement - angleStep, -maxAngleIncrement)
            destAngle = null
        },
        setAngle: function (_angle) {
            destAngle = _angle
            if (destAngle - angle > Math.PI) {
                destAngle -= Math.PI * 2
            } else if (destAngle - angle < -Math.PI) {
                destAngle += Math.PI * 2
            }
        },
        shoot: function (bullets) {
            weapon.shoot(bullets, x, y, angle, speedX, speedY, scale)
        },
        speedX: function () {
            return speedX
        },
        speedY: function () {
            return speedY
        },
        tick: function () {

            if (hitWait) hitWait--

            if (shakeMultiplier > 0) {

                var shakeLevel = 0.01 * shakeMultiplier / maxShakeMultiplier,
                    shakeAngle = Math.random() * Math.PI * 2

                x -= shakeX
                y -= shakeY
                shakeX = shakeLevel * Math.cos(shakeAngle)
                shakeY = shakeLevel * Math.sin(shakeAngle)
                x += shakeX
                y += shakeY

                shakeMultiplier--

            } else {
                shakeX = 0
                shakeY = 0
            }

            weapon.tick()

            if (destAngle === null) angle += angleIncrement
            else angle = (destAngle * 0.08 + angle * 0.92)
            angleIncrement *= 0.95

            angle += hitAngleIncrement
            hitAngleIncrement *= 0.95

            if (gas) {
                speedX += Math.cos(angle) * gasMultiplier
                speedY += Math.sin(angle) * gasMultiplier
            }

            x += speedX
            y += speedY

            that.x = x
            that.y = y

        },
        upgradeWeapon: function () {
            if (weaponIndex < weapons.length - 1) {
                weaponIndex++
                weapon = weapons[weaponIndex]
            }
        },
    }

    return that

}
;
function RepairKit (x, y, speedX, speedY, scale) {

    var randomAngle = Math.random() * Math.PI * 2,
        multiplier = 0.0005
    speedX += Math.cos(randomAngle) * multiplier,
    speedY += Math.sin(randomAngle) * multiplier

    var radius = 0.01,
        scaledRadius = radius * scale

    var life = 1000,
        fadeLife = 10

    var that = {
        x: x,
        y: y,
        radius: radius,
        paint: function (c) {
            c.save()
            c.translate(x * scale, y * scale)
            if (life < fadeLife) c.scale(life / fadeLife, life / fadeLife)
            c.beginPath()
            c.arc(0, 0, scaledRadius, 0, Math.PI * 2)
            c.fillStyle = '#0f4'
            c.fill()
            c.restore()
        },
        resize: function (_scale) {
            scale = _scale
            scaledRadius = radius * scale
        },
        tick: function () {

            x += speedX
            y += speedY

            that.x = x
            that.y = y

            life--
            return life

        },
    }

    return that

}
;
function RepairKitExplosion (x, y) {

    var radius = 0.2

    var life = 0,
        maxLife = 40

    return {
        paint: function (c, scale) {
            c.save()
            c.translate(x * scale, y * scale)
            c.beginPath()
            c.globalAlpha = Math.pow(1 - life / maxLife, 3) * 0.3
            c.arc(0, 0, scale * radius * life / maxLife, 0, Math.PI * 2)
            c.fillStyle = '#0f4'
            c.fill()
            c.restore()
        },
        tick: function () {
            life++
            return life < maxLife
        },
    }

}
;
function ScoreLabel () {

    var x, y, font, labelCanvas

    var textX, textY
    var verticalAlign = 0.55

    var color = 'rgba(153, 153, 153, 0.6)'

    return {
        paint: function (c, score, scoreMultiplier) {

            var text = score
            if (scoreMultiplier > 1) text += ' ' + scoreMultiplier + 'x'

            c.drawImage(labelCanvas, x, y)
            c.fillStyle = color
            c.font = font
            c.textBaseline = 'middle'
            c.fillText(text, textX, textY)

        },
        resize: function (scale) {

            x = scale * 0.05
            y = scale * 0.1

            var fontSize = scale * 0.04
            font = 'bold ' + fontSize + 'px monospace'

            labelCanvas = (function () {

                var padding = 1,
                    text = 'SCORE'

                var canvas = document.createElement('canvas')
                canvas.height = Math.ceil(fontSize + padding * 2)

                var font = fontSize + 'px monospace'

                var c = canvas.getContext('2d')
                c.font = font
                canvas.width = c.measureText(text).width + padding * 2
                c.fillStyle = 'rgba(128, 128, 128, 0.6)'
                c.font = font
                c.textBaseline = 'middle'
                c.fillText(text, padding, canvas.height * verticalAlign)

                return canvas

            })()

            textX = x + labelCanvas.width + scale * 0.02
            textY = y + labelCanvas.height * verticalAlign

        },
    }

}
;
function SimpleWeapon () {

    var wait = 0
    var maxWait = 9

    return {
        shoot: function (bullets, x, y, angle, speedX, speedY, scale) {

            if (wait) return
            wait = maxWait

            bullets.push(Bullet(x, y, angle, speedX, speedY, scale))

            var multiplier = 0.00006
            speedX -= Math.cos(angle) * multiplier
            speedY -= Math.sin(angle) * multiplier

        },
        tick: function () {
            if (wait) wait--
        },
    }

}
;
function StarBackground (distance) {

    var stars = []
    for (var i = 0; i < 80 * distance; i++) {
        stars.push({
            x: Math.random() - 0.5,
            y: Math.random() - 0.5,
            radius: (0.05 + Math.random() * 0.95) / (distance - 6),
        })
    }

    var scale
    var canvasWidth, canvasHeight
    var halfCanvasWidth, halfCanvasHeight

    var fillStyle

    return {
        paint: function (c, x, y) {

            x /= distance
            y /= distance

            c.save()
            c.translate(-x * scale, -y * scale)
            c.fillStyle = fillStyle
            c.fillRect(x * scale - halfCanvasWidth, y * scale - halfCanvasHeight, canvasWidth, canvasHeight)
            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {

            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            halfCanvasHeight = canvasHeight / 2

            var size = Math.ceil(scale)

            var canvas = document.createElement('canvas')
            canvas.width = canvas.height = size

            var c = canvas.getContext('2d')

            for (var i = 0; i < stars.length; i++) {

                var star = stars[i],
                    visualRadius = star.radius * scale * 0.005

                var hue = Math.random() * 360,
                    saturation = 90 + Math.random() * 10,
                    luminance = 70 + Math.random() * 30

                for (var j = -1; j <= 1; j++) {
                    for (var k = -1; k <= 1; k++) {

                        var x = star.x * size + j * size,
                            y = star.y * size + k * size

                        c.fillStyle = 'hsla(' + hue + ', ' + saturation + '%, ' + luminance + '%, 0.2)'
                        c.beginPath()
                        c.arc(x, y, visualRadius * 2, 0, Math.PI * 2)
                        c.fill()

                        c.fillStyle = 'hsla(' + hue + ', ' + saturation + '%, ' + luminance + '%, 1)'
                        c.beginPath()
                        c.arc(x, y, visualRadius, 0, Math.PI * 2)
                        c.fill()

                    }
                }

            }

            c.globalCompositeOperation = 'source-in'
            c.globalAlpha = 0.94
            c.drawImage(canvas, 0, 0)

            fillStyle = c.createPattern(canvas, 'repeat')

        },
    }

}
;
function Start () {

    var maxLife = 15,
        life = maxLife

    var scale,
        canvasWidth,
        canvasHeight,
        halfCanvasWidth,
        font

    var hiding = false

    return {
        hide: function () {
            hiding = true
        },
        paint: function (c) {

            if (!life) return

            c.save()
            c.globalAlpha = life / maxLife

            c.fillStyle = 'rgba(0, 0, 0, 0.8)'
            c.fillRect(0, 0, canvasWidth, canvasHeight)

            c.translate(halfCanvasWidth, 0)

            c.fillStyle = '#fff'
            c.textAlign = 'center'
            c.textBaseline = 'middle'
            c.font = font
            c.fillText('DIRECTION', -halfCanvasWidth + scale * 0.18, canvasHeight - scale * 0.175)
            c.fillText('FIRE', halfCanvasWidth - scale * 0.14, canvasHeight - scale * 0.225)
            c.fillText('GAS', halfCanvasWidth - scale * 0.23, canvasHeight - scale * 0.135)

            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {
            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            font = 'bold ' + (scale * 0.034) + 'px sans-serif'
        },
        tick: function () {
            if (hiding && life) life--
        },
    }

}
;
function WeaponUpgrade (x, y, speedX, speedY, scale) {

    var randomAngle = Math.random() * Math.PI * 2,
        multiplier = 0.0005
    speedX += Math.cos(randomAngle) * multiplier,
    speedY += Math.sin(randomAngle) * multiplier

    var radius = 0.01,
        scaledRadius = radius * scale

    var life = 1000,
        fadeLife = 10

    var that = {
        x: x,
        y: y,
        radius: radius,
        paint: function (c) {
            c.save()
            c.translate(x * scale, y * scale)
            if (life < fadeLife) c.scale(life / fadeLife, life / fadeLife)
            c.beginPath()
            c.arc(0, 0, scaledRadius, 0, Math.PI * 2)
            c.fillStyle = '#f50'
            c.fill()
            c.restore()
        },
        resize: function (_scale) {
            scale = _scale
            scaledRadius = radius * scale
        },
        tick: function () {

            x += speedX
            y += speedY

            that.x = x
            that.y = y

            life--
            return life

        },
    }

    return that

}
;
function WeaponUpgradeExplosion (x, y) {

    var radius = 0.2

    var life = 0,
        maxLife = 40

    return {
        paint: function (c, scale) {
            c.save()
            c.translate(x * scale, y * scale)
            c.beginPath()
            c.globalAlpha = Math.pow(1 - life / maxLife, 3) * 0.3
            c.arc(0, 0, scale * radius * life / maxLife, 0, Math.PI * 2)
            c.fillStyle = '#f50'
            c.fill()
            c.restore()
        },
        tick: function () {
            life++
            return life < maxLife
        },
    }

}
;
addEventListener('load', function () {
    var mainCanvas = MainCanvas()
    document.body.appendChild(mainCanvas.element)
    document.addEventListener('visibilitychange', function () {
        if (document.visibilityState == 'visible') mainCanvas.resume()
        else mainCanvas.pause()
    })
})
;

})()