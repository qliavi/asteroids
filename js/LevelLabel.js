function LevelLabel () {

    var x, y, font, labelCanvas

    var textX, textY
    var verticalAlign = 0.55

    var color = 'rgba(153, 153, 153, 0.6)'

    return {
        paint: function (c, level) {

            var text = level

            c.drawImage(labelCanvas, x, y)
            c.fillStyle = color
            c.font = font
            c.textBaseline = 'middle'
            c.fillText(text, textX, textY)

        },
        resize: function (scale) {

            x = scale * 0.05
            y = scale * 0.16

            var fontSize = scale * 0.04
            font = 'bold ' + fontSize + 'px monospace'

            labelCanvas = (function () {

                var padding = 1,
                    text = 'LEVEL'

                var canvas = document.createElement('canvas')
                canvas.height = Math.ceil(fontSize + padding * 2)

                var font = fontSize + 'px monospace'

                var c = canvas.getContext('2d')
                c.font = font
                canvas.width = c.measureText(text).width + padding * 2
                c.fillStyle = 'rgba(128, 128, 128, 0.6)'
                c.font = font
                c.textBaseline = 'middle'
                c.fillText(text, padding, canvas.height * verticalAlign)

                return canvas

            })()

            textX = x + labelCanvas.width + scale * 0.02
            textY = y + labelCanvas.height * verticalAlign

        },
    }

}
