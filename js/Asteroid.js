function Asteroid (x, y, size, scale, canvasWidth, canvasHeight, speedX, speedY) {

    var randomAngle = Math.random() * Math.PI * 2,
        speedMultiplier = 0.003 / size
    speedX += Math.cos(randomAngle) * speedMultiplier,
    speedY += Math.sin(randomAngle) * speedMultiplier

    var maxLife = size,
        life = maxLife

    var radius = size * 0.01,
        scaledRadius = radius * scale

    var maxShakeMultiplier = 40,
        shakeMultiplier = 0

    var visualX, visualY

    var that = {
        radius: radius,
        size: size,
        x: x,
        y: y,
        hit: function () {
            shakeMultiplier = maxShakeMultiplier
            life--
            return life > 0
        },
        paint: function (c) {
            c.save()
            c.translate(visualX * scale, visualY * scale)
            c.moveTo(scaledRadius, 0)
            c.arc(0, 0, scaledRadius, 0, Math.PI * 2)
            c.restore()
        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {
            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            scaledRadius = radius * scale
        },
        speedX: function () {
            return speedX
        },
        speedY: function () {
            return speedY
        },
        tick: function (planeX, planeY) {

            if (shakeMultiplier > 0) {

                var shakeLevel = Math.pow(1 - life / maxLife, 2) * 0.01 * shakeMultiplier / maxShakeMultiplier,
                    shakeAngle = Math.random() * Math.PI * 2

                visualX = x + shakeLevel * Math.cos(shakeAngle)
                visualY = y + shakeLevel * Math.sin(shakeAngle)

                shakeMultiplier--

            } else {
                visualX = x
                visualY = y
            }

            x += speedX
            y += speedY

            var sceneWidth = canvasWidth / scale,
                edgeX = sceneWidth / 2 + radius

            if (x - planeX > edgeX) {
                x -= sceneWidth + radius * 2
            }

            if (x - planeX < -edgeX) {
                x += sceneWidth + radius * 2
            }

            var sceneHeight = canvasHeight / scale,
                edgeY = sceneHeight / 2 + radius

            if (y - planeY > edgeY) {
                y -= sceneHeight + radius * 2
            }

            if (y - planeY < -edgeY) {
                y += sceneHeight + radius * 2
            }

            that.x = x
            that.y = y

        },
    }

    return that

}
