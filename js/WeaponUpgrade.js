function WeaponUpgrade (x, y, speedX, speedY, scale) {

    var randomAngle = Math.random() * Math.PI * 2,
        multiplier = 0.0005
    speedX += Math.cos(randomAngle) * multiplier,
    speedY += Math.sin(randomAngle) * multiplier

    var radius = 0.01,
        scaledRadius = radius * scale

    var life = 1000,
        fadeLife = 10

    var that = {
        x: x,
        y: y,
        radius: radius,
        paint: function (c) {
            c.save()
            c.translate(x * scale, y * scale)
            if (life < fadeLife) c.scale(life / fadeLife, life / fadeLife)
            c.beginPath()
            c.arc(0, 0, scaledRadius, 0, Math.PI * 2)
            c.fillStyle = '#f50'
            c.fill()
            c.restore()
        },
        resize: function (_scale) {
            scale = _scale
            scaledRadius = radius * scale
        },
        tick: function () {

            x += speedX
            y += speedY

            that.x = x
            that.y = y

            life--
            return life

        },
    }

    return that

}
