function LevelClear () {

    var life = 0
    var lifeDirection = 0
    var maxLife = 15

    var scale,
        fontSize

    var font

    var canvasWidth,
        canvasHeight

    var halfCanvasWidth,
        halfCanvasHeight

    var animationWidth

    var that = {
        canHide: false,
        hide: function () {
            lifeDirection = -1
        },
        paint: function (c) {

            if (!life) return

            var lifeRatio = life / maxLife,
                invertedLifeRatio = 1 - lifeRatio

            c.save()
            c.globalAlpha = lifeRatio

            c.fillStyle = 'rgba(0, 0, 0, 0.8)'
            c.fillRect(0, 0, canvasWidth, canvasHeight)

            c.translate(halfCanvasWidth, halfCanvasHeight)

            c.fillStyle = '#fff'
            c.textAlign = 'center'
            c.textBaseline = 'middle'

            c.font = font
            c.fillText('LEVEL CLEAR', -invertedLifeRatio * animationWidth, 0)

            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {
            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            halfCanvasHeight = canvasHeight / 2
            fontSize = scale * 0.05
            font = 'bold ' + fontSize + 'px sans-serif'
            animationWidth = scale * 0.2
        },
        show: function () {
            lifeDirection = 1
        },
        tick: function () {

            life += lifeDirection

            if (life == maxLife) {
                that.canHide = true
                lifeDirection = 0
            } else {
                that.canHide = false
                if (!life) lifeDirection = 0
            }

        },
    }

    return that

}
