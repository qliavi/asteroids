function StarBackground (distance) {

    var stars = []
    for (var i = 0; i < 80 * distance; i++) {
        stars.push({
            x: Math.random() - 0.5,
            y: Math.random() - 0.5,
            radius: (0.05 + Math.random() * 0.95) / (distance - 6),
        })
    }

    var scale
    var canvasWidth, canvasHeight
    var halfCanvasWidth, halfCanvasHeight

    var fillStyle

    return {
        paint: function (c, x, y) {

            x /= distance
            y /= distance

            c.save()
            c.translate(-x * scale, -y * scale)
            c.fillStyle = fillStyle
            c.fillRect(x * scale - halfCanvasWidth, y * scale - halfCanvasHeight, canvasWidth, canvasHeight)
            c.restore()

        },
        resize: function (_scale, _canvasWidth, _canvasHeight) {

            scale = _scale
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            halfCanvasWidth = canvasWidth / 2
            halfCanvasHeight = canvasHeight / 2

            var size = Math.ceil(scale)

            var canvas = document.createElement('canvas')
            canvas.width = canvas.height = size

            var c = canvas.getContext('2d')

            for (var i = 0; i < stars.length; i++) {

                var star = stars[i],
                    visualRadius = star.radius * scale * 0.005

                var hue = Math.random() * 360,
                    saturation = 90 + Math.random() * 10,
                    luminance = 70 + Math.random() * 30

                for (var j = -1; j <= 1; j++) {
                    for (var k = -1; k <= 1; k++) {

                        var x = star.x * size + j * size,
                            y = star.y * size + k * size

                        c.fillStyle = 'hsla(' + hue + ', ' + saturation + '%, ' + luminance + '%, 0.2)'
                        c.beginPath()
                        c.arc(x, y, visualRadius * 2, 0, Math.PI * 2)
                        c.fill()

                        c.fillStyle = 'hsla(' + hue + ', ' + saturation + '%, ' + luminance + '%, 1)'
                        c.beginPath()
                        c.arc(x, y, visualRadius, 0, Math.PI * 2)
                        c.fill()

                    }
                }

            }

            c.globalCompositeOperation = 'source-in'
            c.globalAlpha = 0.94
            c.drawImage(canvas, 0, 0)

            fillStyle = c.createPattern(canvas, 'repeat')

        },
    }

}
