addEventListener('load', function () {
    var mainCanvas = MainCanvas()
    document.body.appendChild(mainCanvas.element)
    document.addEventListener('visibilitychange', function () {
        if (document.visibilityState == 'visible') mainCanvas.resume()
        else mainCanvas.pause()
    })
})
