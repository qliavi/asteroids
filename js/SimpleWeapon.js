function SimpleWeapon () {

    var wait = 0
    var maxWait = 9

    return {
        shoot: function (bullets, x, y, angle, speedX, speedY, scale) {

            if (wait) return
            wait = maxWait

            bullets.push(Bullet(x, y, angle, speedX, speedY, scale))

            var multiplier = 0.00006
            speedX -= Math.cos(angle) * multiplier
            speedY -= Math.sin(angle) * multiplier

        },
        tick: function () {
            if (wait) wait--
        },
    }

}
