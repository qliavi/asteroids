function DoubleWeapon () {

    var wait = 0
    var maxWait = 9

    var distance = 0.014

    return {
        shoot: function (bullets, x, y, angle, speedX, speedY, scale) {

            if (wait) return
            wait = maxWait

            var sideAngle = angle + Math.PI / 2,
                bulletX = x + Math.cos(sideAngle) * distance,
                bulletY = y + Math.sin(sideAngle) * distance
            bullets.push(Bullet(bulletX, bulletY, angle, speedX, speedY, scale))

            var sideAngle = angle - Math.PI / 2,
                bulletX = x + Math.cos(sideAngle) * distance,
                bulletY = y + Math.sin(sideAngle) * distance
            bullets.push(Bullet(bulletX, bulletY, angle, speedX, speedY, scale))

            var multiplier = 0.00006
            speedX -= Math.cos(angle) * multiplier
            speedY -= Math.sin(angle) * multiplier

        },
        tick: function () {
            if (wait) wait--
        },
    }

}
