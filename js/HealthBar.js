function HealthBar () {

    var x, y, width, height

    return {
        paint: function (c, lifeRatio) {

            c.fillStyle = 'rgba(255, 255, 255, 0.1)'
            c.fillRect(x, y, width, height)

            c.fillStyle = 'hsla(' + Math.floor(lifeRatio * 120) + ', 100%, 50%, 0.8)'
            c.fillRect(x, y, width * lifeRatio, height)

        },
        resize: function (scale, canvasWidth) {
            x = scale * 0.05
            y = scale * 0.05
            width = canvasWidth - 0.10 * scale
            height = scale * 0.01
        },
    }

}
