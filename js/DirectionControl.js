function DirectionControl (canvas) {

    function updateAngle () {

        controlX = touchX - x,
        controlY = touchY - y

        angle = Math.atan(controlY / controlX)
        if (controlX < 0) angle += Math.PI

        var distance = Math.sqrt(controlX * controlX + controlY * controlY)
        if (distance > radius) distance = radius

        controlX = Math.cos(angle) * distance
        controlY = Math.sin(angle) * distance

    }

    var scale, devicePixelRatio

    var angle = null
    var x, y, radius

    var identifier = null
    var touchX, touchY

    var controlX = 0,
        controlY = 0

    canvas.addEventListener('touchend', function (e) {
        e.preventDefault()
        if (identifier === null) return
        var touches = e.changedTouches
        for (var i = 0; i < touches.length; i++) {
            if (touches[i].identifier === identifier) {
                identifier = null
                angle = null
                controlX = controlY = 0
                break
            }
        }
    })
    canvas.addEventListener('touchmove', function (e) {
        e.preventDefault()
        if (identifier === null) return
        var touches = e.changedTouches
        for (var i = 0; i < touches.length; i++) {
            var touch = touches[i]
            if (touch.identifier === identifier) {
                touchX = touch.clientX * devicePixelRatio
                touchY = touch.clientY * devicePixelRatio
                updateAngle()
                break
            }
        }
    })
    canvas.addEventListener('touchstart', function (e) {
        e.preventDefault()
        if (identifier !== null) return
        var touches = e.changedTouches
        for (var i = 0; i < touches.length; i++) {

            var touch = touches[i],
                clientX = touch.clientX * devicePixelRatio,
                clientY = touch.clientY * devicePixelRatio

            var dx = clientX - x,
                dy = clientY - y

            if (Math.sqrt(dx * dx + dy * dy) < radius) {
                identifier = touch.identifier
                touchX = clientX
                touchY = clientY
                updateAngle()
                break
            }

        }
    })

    return {
        angle: function () {
            return angle
        },
        paint: function (c) {

            c.save()
            c.translate(x, y)

            c.lineWidth = scale * 0.004
            if (identifier === null) {
                c.fillStyle = 'hsla(60, 60%, 50%, 0.1)'
                c.strokeStyle = 'hsla(60, 100%, 50%, 0.4)'
            } else {
                c.fillStyle = 'hsla(60, 60%, 50%, 0.4)'
                c.strokeStyle = 'hsla(60, 100%, 50%, 0.6)'
            }

            c.beginPath()
            c.arc(0, 0, radius, 0, Math.PI * 2)
            c.fill()
            c.stroke()

            c.beginPath()
            c.arc(controlX, controlY, radius / 3, 0, Math.PI * 2)
            c.fill()
            c.stroke()

            c.restore()

        },
        resize: function (_scale, _devicePixelRatio, canvasWidth, canvasHeight) {
            scale = _scale
            devicePixelRatio = _devicePixelRatio
            radius = scale * 0.13
            x = scale * 0.05 + radius
            y = canvasHeight - (scale * 0.05 + radius)
            if (identifier !== null) updateAngle()
        },
    }

}
