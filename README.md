Asteroids
=========

A space shooter game.

The app is available at http://asteroids.qliavi.com/.

For technical support create an issue or contact us at info@qliavi.com.
